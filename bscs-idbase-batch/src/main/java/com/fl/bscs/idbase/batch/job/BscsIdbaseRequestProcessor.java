package com.fl.bscs.idbase.batch.job;

import com.fl.bscs.idbase.application.usecase.BscsIdbaseRequestUsecase;
import com.fl.bscs.idbase.domain.aggregate.BscsIdbaseRequest;
import com.fl.bscs.idbase.domain.vo.Status;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.experimental.FieldDefaults;
import lombok.extern.slf4j.Slf4j;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;

@Slf4j
@Service
@AllArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
public class BscsIdbaseRequestProcessor implements ItemProcessor<List<BscsIdbaseRequest>, List<BscsIdbaseRequest>> {

    BscsIdbaseRequestUsecase bscsIdbaseRequestUsecase;

    @Override
    public List<BscsIdbaseRequest> process(List<BscsIdbaseRequest> bscsIdbaseRequests) {

        bscsIdbaseRequests.forEach(bscsIdbaseRequest -> {
                    try {
                        bscsIdbaseRequestUsecase.mettreAJourBscsIdbaseRequest(bscsIdbaseRequest);
                        bscsIdbaseRequest.setStatus(Status.SUCCES);
                        bscsIdbaseRequest.setStatusDate(LocalDateTime.now());
                        bscsIdbaseRequest.setErrorMessages("");
                    } catch (Exception ex) {
                        bscsIdbaseRequest.setStatus(Status.ERREUR);
                        bscsIdbaseRequest.setStatusDate(LocalDateTime.now());
                        bscsIdbaseRequest.setErrorMessages(ex.getMessage().length() < 100 ? ex.getMessage() : ex.getMessage().substring(0, 100));
                        bscsIdbaseRequest.setRetry(bscsIdbaseRequest.getRetry() != null ? bscsIdbaseRequest.getRetry() + 1 : 0L);
                    }
                }
        );
        return bscsIdbaseRequests;
    }

}
