package com.fl.bscs.idbase.batch.job;

import com.fl.bscs.idbase.domain.aggregate.BscsIdbaseRequest;
import com.fl.bscs.idbase.domain.service.BscsIdbaseRequestService;
import lombok.AccessLevel;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import lombok.extern.slf4j.Slf4j;
import org.springframework.batch.item.ItemReader;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
@Slf4j
public class BscsIdbaseRequestReader implements ItemReader<List<BscsIdbaseRequest>> {

    boolean batchJobState = false;

    @NonNull
    final BscsIdbaseRequestService bscsIdbaseRequestService;

    @Override
    public List<BscsIdbaseRequest> read() {

        if (!batchJobState) {
            batchJobState = true;
            return bscsIdbaseRequestService.getBscsIdbaseRequests();
        }
        {
            batchJobState = false;
            return null;
        }

    }
}
