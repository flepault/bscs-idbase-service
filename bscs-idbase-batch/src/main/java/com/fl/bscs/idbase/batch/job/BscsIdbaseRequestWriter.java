package com.fl.bscs.idbase.batch.job;

import com.fl.bscs.idbase.domain.aggregate.BscsIdbaseRequest;
import com.fl.bscs.idbase.domain.service.BscsIdbaseRequestService;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.batch.item.adapter.ItemWriterAdapter;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@AllArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
public class BscsIdbaseRequestWriter {

    BscsIdbaseRequestService bscsIdbaseRequestService;

    public ItemWriterAdapter<List<BscsIdbaseRequest>> getWriter() {

        ItemWriterAdapter<List<BscsIdbaseRequest>> writer = new ItemWriterAdapter<>();

        writer.setTargetObject(bscsIdbaseRequestService);
        writer.setTargetMethod("saveBscsIdbaseRequests");

        return writer;

    }

}
