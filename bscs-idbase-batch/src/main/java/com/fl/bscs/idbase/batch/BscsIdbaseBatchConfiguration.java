package com.fl.bscs.idbase.batch;

import com.fl.bscs.idbase.batch.job.BscsIdbaseRequestProcessor;
import com.fl.bscs.idbase.batch.job.BscsIdbaseRequestReader;
import com.fl.bscs.idbase.batch.job.BscsIdbaseRequestWriter;
import com.fl.bscs.idbase.batch.job.RemoveSpringBatchHistoryTasklet;
import com.fl.bscs.idbase.domain.BscsIdbaseDomainConfiguration;
import com.fl.bscs.idbase.domain.aggregate.BscsIdbaseRequest;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.batch.core.*;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.batch.core.repository.JobExecutionAlreadyRunningException;
import org.springframework.batch.core.repository.JobInstanceAlreadyCompleteException;
import org.springframework.batch.core.repository.JobRestartException;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;

import java.time.LocalDateTime;
import java.util.List;

@Configuration
@EnableScheduling
@EnableBatchProcessing
@AllArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
@ComponentScan(basePackageClasses = BscsIdbaseRequestReader.class)
@Import({BscsIdbaseDomainConfiguration.class})
public class BscsIdbaseBatchConfiguration {

    JobLauncher jobLauncher;
    JobBuilderFactory jobBuilderFactory;
    StepBuilderFactory stepBuilderFactory;
    BscsIdbaseRequestReader bscsIdbaseRequestReader;
    BscsIdbaseRequestProcessor bscsIdbaseRequestProcessor;
    BscsIdbaseRequestWriter bscsIdbaseRequestWriter;
    RemoveSpringBatchHistoryTasklet removeSpringBatchHistoryTasklet;

    @Scheduled(initialDelay = 2000, fixedDelay = 10000)
    public void launchJob() throws JobParametersInvalidException, JobExecutionAlreadyRunningException, JobRestartException, JobInstanceAlreadyCompleteException {

        JobParameters jobParameters = new JobParametersBuilder().addString("time", LocalDateTime.now().toString()).toJobParameters();

        jobLauncher.run(createJob(), jobParameters);
    }

    @Bean
    public Job createJob() {
        return jobBuilderFactory.get("BscsIdbaseRequestJob")
                .incrementer(new RunIdIncrementer())
                .start(cleanHistoricStep())
                .next(createStepBscsIdbaseRequest())
                .build();
    }

    @Bean
    public Step createStepBscsIdbaseRequest() {
        return stepBuilderFactory.get("BscsIdbaseRequest")
                .<List<BscsIdbaseRequest>, List<BscsIdbaseRequest>>chunk(1)
                .faultTolerant()
                .skipLimit(Integer.MAX_VALUE).skip(Exception.class)
                .reader(bscsIdbaseRequestReader)
                .processor(bscsIdbaseRequestProcessor)
                .writer(bscsIdbaseRequestWriter.getWriter())
                .build();
    }

    @Bean
    public Step cleanHistoricStep() {
        return stepBuilderFactory.get("cleanHistoricStep")
                .tasklet(removeSpringBatchHistoryTasklet)
                .build();
    }
}
