package com.fl.bscs.idbase.domain.vo;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.FieldDefaults;

@AllArgsConstructor
@Getter
@Setter
@FieldDefaults(makeFinal = true, level = AccessLevel.PRIVATE)
public class UpdateBscsDTO {
    Integer operationId;
    String phoneNum;
    String phoneId;
    String typeLabel;
    String civility;
    String lastName;
    String firstName;
    String birthDate;
    String birthPlace;
    String postlAdress;
    String officePhone;
    String homePhone;
    String email;
    String town;
    String commune;
    String block;
    String occupation;
    String scanUser;
    Integer resId;
    Integer omStatus;
}
