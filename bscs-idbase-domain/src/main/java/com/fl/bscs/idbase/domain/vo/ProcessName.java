package com.fl.bscs.idbase.domain.vo;

public enum ProcessName {
    changeMsisdn, changeSim, desactivate
}
