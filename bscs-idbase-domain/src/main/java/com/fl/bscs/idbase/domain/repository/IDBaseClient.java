package com.fl.bscs.idbase.domain.repository;

import com.fl.bscs.idbase.domain.aggregate.BscsIdbaseRequest;

public interface IDBaseClient {

    void setOperationOnNumber(BscsIdbaseRequest bscsIdbaseRequest) throws Exception;

}
