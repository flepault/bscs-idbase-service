package com.fl.bscs.idbase.domain.repository;

import com.fl.bscs.idbase.domain.aggregate.BscsIdbaseRequest;
import com.fl.bscs.idbase.domain.vo.Status;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface BscsIdbaseRequestRepository extends JpaRepository<BscsIdbaseRequest, Long> {

    @Query(value = "SELECT r FROM BscsIdbaseRequest r WHERE r.status = :nouveau or (r.status = :erreur and r.retry < 3)")
    List<BscsIdbaseRequest> findByStatus(Status nouveau, Status erreur);

}
