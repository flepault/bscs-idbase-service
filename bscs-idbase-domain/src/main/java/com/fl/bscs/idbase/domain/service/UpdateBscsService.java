package com.fl.bscs.idbase.domain.service;

import com.fl.bscs.idbase.domain.repository.UpdateBscsResultRepository;
import com.fl.bscs.idbase.domain.vo.UpdateBscsDTO;
import com.fl.bscs.idbase.domain.vo.UpdateBscsResultDTO;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.stereotype.Service;

import java.util.Map;

@Service
@AllArgsConstructor
@FieldDefaults(makeFinal = true, level = AccessLevel.PRIVATE)
public class UpdateBscsService {

    UpdateBscsResultRepository updateBscsResultRepository;

    public UpdateBscsResultDTO updateBscs(UpdateBscsDTO updateBscsDTO) {

        Map<String, Object> result = updateBscsResultRepository.updateBscs(
                updateBscsDTO.getOperationId(),
                updateBscsDTO.getPhoneId(),
                updateBscsDTO.getPhoneNum(),
                updateBscsDTO.getTypeLabel(),
                updateBscsDTO.getCivility(),
                updateBscsDTO.getLastName(),
                updateBscsDTO.getFirstName(),
                updateBscsDTO.getBirthDate(),
                updateBscsDTO.getBirthPlace(),
                updateBscsDTO.getPostlAdress(),
                updateBscsDTO.getOfficePhone(),
                updateBscsDTO.getHomePhone(),
                updateBscsDTO.getEmail(),
                updateBscsDTO.getTown(),
                updateBscsDTO.getCommune(),
                updateBscsDTO.getBlock(),
                updateBscsDTO.getOccupation(),
                updateBscsDTO.getScanUser(),
                updateBscsDTO.getResId(),
                updateBscsDTO.getOmStatus()
        );

        String msisdn = (String) result.get("posMsisdn");
        String sim = (String) result.get("posSIM");

        if (msisdn == null) {
            throw new RuntimeException("MSISDN non existant");
        }

        if (sim == null) {
            throw new RuntimeException("SIM non existant");
        }

        return UpdateBscsResultDTO.of(msisdn, sim);
    }
}
