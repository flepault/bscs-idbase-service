package com.fl.bscs.idbase.domain.service;

import com.fl.bscs.idbase.domain.aggregate.BscsIdbaseRequest;
import com.fl.bscs.idbase.domain.repository.BscsIdbaseRequestRepository;
import com.fl.bscs.idbase.domain.vo.Status;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.experimental.FieldDefaults;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Service;

import java.util.List;

@Slf4j
@Service
@AllArgsConstructor
@FieldDefaults(makeFinal = true, level = AccessLevel.PRIVATE)
public class BscsIdbaseRequestService {

    BscsIdbaseRequestRepository bscsIdbaseRequestRepository;

    public List<BscsIdbaseRequest> getBscsIdbaseRequests() {
        return bscsIdbaseRequestRepository.findByStatus(Status.NOUVEAU, Status.ERREUR);
    }

    public BscsIdbaseRequest ajouterBscsIdbaseRequest(BscsIdbaseRequest bscsIdbaseRequest) {
        return bscsIdbaseRequestRepository.save(bscsIdbaseRequest);
    }

    public void saveBscsIdbaseRequests(List<BscsIdbaseRequest> bscsIdbaseRequests) {
        bscsIdbaseRequests.forEach(bscsIdbaseRequestRepository::save);
    }

}
