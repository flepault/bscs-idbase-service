package com.fl.bscs.idbase.domain.jpa.hibernate;

import org.hibernate.boot.model.naming.Identifier;
import org.hibernate.engine.jdbc.env.spi.JdbcEnvironment;
import org.springframework.boot.orm.jpa.hibernate.SpringPhysicalNamingStrategy;

public class PhysicalNamingStrategy extends SpringPhysicalNamingStrategy {

    private static final String PREFIX_TABLE_DB = "";

    @Override
    public Identifier toPhysicalTableName(Identifier name, JdbcEnvironment context) {
        if (name.getText().toUpperCase().startsWith(PREFIX_TABLE_DB)) {
            return super.toPhysicalTableName(new Identifier(name.getText(), name.isQuoted()), context);
        } else {
            return super.toPhysicalTableName(new Identifier(PREFIX_TABLE_DB + name.getText(), name.isQuoted()), context);
        }
    }
}
