package com.fl.bscs.idbase.domain;

import com.fl.bscs.idbase.domain.aggregate.BscsIdbaseRequest;
import com.fl.bscs.idbase.domain.entity.UpdateBscsResult;
import com.fl.bscs.idbase.domain.repository.BscsIdbaseRequestRepository;
import com.fl.bscs.idbase.domain.repository.UpdateBscsResultRepository;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@Configuration
@ComponentScan
@EntityScan(
        basePackageClasses = {BscsIdbaseRequest.class, UpdateBscsResult.class}
)
@EnableJpaRepositories(
        basePackageClasses = {BscsIdbaseRequestRepository.class, UpdateBscsResultRepository.class}
)
public class BscsIdbaseDomainConfiguration {
}
