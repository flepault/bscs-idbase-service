package com.fl.bscs.idbase.domain.aggregate;

import com.fl.bscs.idbase.domain.vo.ProcessName;
import com.fl.bscs.idbase.domain.vo.Status;
import lombok.*;
import lombok.experimental.Accessors;
import lombok.experimental.FieldDefaults;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;

@Entity
@Setter
@Getter
@FieldDefaults(level = AccessLevel.PRIVATE)
@Accessors(chain = true)
@RequiredArgsConstructor(staticName = "of")
@NoArgsConstructor
public class BscsIdbaseRequest {

    @Id
    @NonNull
    @NotNull
    Long requestId;

    @NonNull
    @NotNull
    Long coId;

    @NonNull
    @NotNull
    LocalDateTime insertDate;

    @NonNull
    @NotNull
    @Enumerated(EnumType.ORDINAL)
    Status status;

    @NonNull
    @NotNull
    LocalDateTime statusDate;

    Long retry;

    @NonNull
    @NotNull
    @Enumerated(EnumType.STRING)
    ProcessName processName;

    String msisdn;

    String oldMsisdn;

    String sim;

    String oldSim;

    String partyId;

    @NonNull
    @NotNull
    String username;

    String errorMessages;

}
