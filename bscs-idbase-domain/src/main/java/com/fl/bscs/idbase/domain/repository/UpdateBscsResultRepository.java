package com.fl.bscs.idbase.domain.repository;

import com.fl.bscs.idbase.domain.entity.UpdateBscsResult;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.query.Procedure;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Map;

@Repository
public interface UpdateBscsResultRepository extends JpaRepository<UpdateBscsResult, Long> {

    @Procedure(name = "idbase.UpdateBscs")
    Map<String, Object> updateBscs(
            @Param("pinOperationId") Integer pinOperationId,
            @Param("pisPhoneId") String pisPhoneId,
            @Param("pisPhoneNum") String pisPhoneNum,
            @Param("pisTypeLabel") String pisTypeLabel,
            @Param("pisCivility") String pisCivility,
            @Param("pisLastName") String pisLastName,
            @Param("pisFirstName") String pisFirstName,
            @Param("pisBirthDate") String pisBirthDate,
            @Param("pisBirthPlace") String pisBirthPlace,
            @Param("pisPostlAdress") String pisPostlAdress,
            @Param("pisHomePhone") String pisHomePhone,
            @Param("pisOfficePhone") String pisOfficePhone,
            @Param("pisEmail") String pisEmail,
            @Param("pisTown") String pisTown,
            @Param("pisCommune") String pisCommune,
            @Param("pisBlock") String pisBlock,
            @Param("pisOccupation") String pisOccupation,
            @Param("pisScanUser") String pisScanUser,
            @Param("pionResId") Integer pionResId,
            @Param("pionOMStatus") Integer pionOMStatus
    );

}
