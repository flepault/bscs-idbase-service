package com.fl.bscs.idbase.domain.entity;

import lombok.*;

import javax.persistence.*;
import java.io.Serializable;

@NamedStoredProcedureQueries({
        @NamedStoredProcedureQuery(
                name = "idbase.UpdateBscs",
                procedureName = "idbase.UpdateBscs",
                parameters = {
                        @StoredProcedureParameter(mode = ParameterMode.IN, type = Integer.class, name = "pinOperationId"),
                        @StoredProcedureParameter(mode = ParameterMode.IN, type = String.class, name = "pisPhoneId"),
                        @StoredProcedureParameter(mode = ParameterMode.IN, type = String.class, name = "pisPhoneNum"),
                        @StoredProcedureParameter(mode = ParameterMode.IN, type = String.class, name = "pisTypeLabel"),
                        @StoredProcedureParameter(mode = ParameterMode.IN, type = String.class, name = "pisCivility"),
                        @StoredProcedureParameter(mode = ParameterMode.IN, type = String.class, name = "pisLastName"),
                        @StoredProcedureParameter(mode = ParameterMode.IN, type = String.class, name = "pisFirstName"),
                        @StoredProcedureParameter(mode = ParameterMode.IN, type = String.class, name = "pisBirthDate"),
                        @StoredProcedureParameter(mode = ParameterMode.IN, type = String.class, name = "pisBirthPlace"),
                        @StoredProcedureParameter(mode = ParameterMode.IN, type = String.class, name = "pisPostlAdress"),
                        @StoredProcedureParameter(mode = ParameterMode.IN, type = String.class, name = "pisOfficePhone"),
                        @StoredProcedureParameter(mode = ParameterMode.IN, type = String.class, name = "pisHomePhone"),
                        @StoredProcedureParameter(mode = ParameterMode.IN, type = String.class, name = "pisEmail"),
                        @StoredProcedureParameter(mode = ParameterMode.IN, type = String.class, name = "pisTown"),
                        @StoredProcedureParameter(mode = ParameterMode.IN, type = String.class, name = "pisCommune"),
                        @StoredProcedureParameter(mode = ParameterMode.IN, type = String.class, name = "pisBlock"),
                        @StoredProcedureParameter(mode = ParameterMode.IN, type = String.class, name = "pisOccupation"),
                        @StoredProcedureParameter(mode = ParameterMode.IN, type = String.class, name = "pisScanUser"),
                        @StoredProcedureParameter(mode = ParameterMode.IN, type = Integer.class, name = "pionResId"),
                        @StoredProcedureParameter(mode = ParameterMode.IN, type = Integer.class, name = "pionOMStatus"),
                        @StoredProcedureParameter(mode = ParameterMode.OUT, type = String.class, name = "posMsisdn"),
                        @StoredProcedureParameter(mode = ParameterMode.OUT, type = String.class, name = "posSIM")
                }
        )
})
@NoArgsConstructor
@AllArgsConstructor(staticName = "of")
@Getter
@Setter
@Entity
public class UpdateBscsResult implements Serializable {
    @Id @GeneratedValue
    private Long id;
}
