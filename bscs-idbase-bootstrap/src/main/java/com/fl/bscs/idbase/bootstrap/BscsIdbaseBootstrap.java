package com.fl.bscs.idbase.bootstrap;

import com.fl.bscs.idbase.batch.BscsIdbaseBatchConfiguration;
import com.fl.bscs.idbase.bootstrap.configuration.FlywayConfig;
import com.fl.bscs.idbase.data.soap.BscsIdbaseDataSoapConfiguration;
import com.fl.bscs.idbase.rest.BscsIdbaseRestConfiguration;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.error.ErrorController;
import org.springframework.context.annotation.Import;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Slf4j
@Controller
@SpringBootApplication
@Import(
        {
                FlywayConfig.class,
                FlywayConfig.FlywayInitializerJpaDependencyConfiguration.class,
                BscsIdbaseRestConfiguration.class,
                BscsIdbaseBatchConfiguration.class,
                BscsIdbaseDataSoapConfiguration.class
        }
)
public class BscsIdbaseBootstrap implements ErrorController {

    public static void main(String[] args) {
        SpringApplication.run(BscsIdbaseBootstrap.class, args);
    }

    private static final String PATH = "/error";

    @RequestMapping(value = PATH)
    public String error() {
        return "forward:/index.html";
    }

    @Override
    public String getErrorPath() {
        return PATH;
    }

}
