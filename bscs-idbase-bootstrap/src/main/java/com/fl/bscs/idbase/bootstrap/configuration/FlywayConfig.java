package com.fl.bscs.idbase.bootstrap.configuration;

import org.flywaydb.core.Flyway;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.data.jpa.EntityManagerFactoryDependsOnPostProcessor;
import org.springframework.boot.autoconfigure.flyway.FlywayMigrationInitializer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.sql.DataSource;

@Configuration
public
class FlywayConfig {

    @Bean
    Flyway flyway(DataSource dataSource,
                  @Value("${spring.flyway.baseline-on-migrate}") boolean baselineOnMigrate,
                  @Value("${spring.flyway.table}") String table,
                  @Value("${spring.flyway.locations}") String location
                  ) {
        Flyway flyway = new Flyway();
        flyway.setDataSource(dataSource);
        flyway.setBaselineOnMigrate(baselineOnMigrate);
        flyway.setLocations(location);
        flyway.setTable(table);
        return flyway;
    }

    @Bean
    FlywayMigrationInitializer flywayInitializer(Flyway flyway) {
        return new FlywayMigrationInitializer(flyway, null);
    }

    @Configuration
    public static class FlywayInitializerJpaDependencyConfiguration
            extends EntityManagerFactoryDependsOnPostProcessor {
        public FlywayInitializerJpaDependencyConfiguration() { super("flywayInitializer"); }
    }
}
