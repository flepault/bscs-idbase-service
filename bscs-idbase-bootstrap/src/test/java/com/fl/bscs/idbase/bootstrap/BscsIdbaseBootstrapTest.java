package com.fl.bscs.idbase.bootstrap;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.test.context.ActiveProfiles;

@SpringBootApplication
@ActiveProfiles("db_oracle_xe")
class BscsIdbaseBootstrapTest {

    public static void main(String[] args) {
        SpringApplication.run(BscsIdbaseBootstrap.class, args);
    }
}
