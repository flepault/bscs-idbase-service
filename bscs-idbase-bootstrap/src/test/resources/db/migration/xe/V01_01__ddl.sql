CREATE TABLE IDBASEBSCS.BSCS_IDBASE_REQUEST
(
    REQUEST_ID      INTEGER                 NOT NULL,
    CO_ID           INTEGER                 NOT NULL,
    INSERT_DATE     DATE                    NOT NULL,
    STATUS          INTEGER                 NOT NULL,
    STATUS_DATE     DATE                    NOT NULL,
    RETRY           INTEGER                 NULL,
    PROCESS_NAME    VARCHAR2(30 BYTE)       NOT NULL,
    MSISDN          VARCHAR2(20 BYTE)       NULL,
    OLD_MSISDN      VARCHAR2(20 BYTE)       NULL,
    SIM             VARCHAR2(20 BYTE)       NULL,
    OLD_SIM         VARCHAR2(20 BYTE)       NULL,
    PARTY_ID        VARCHAR2(20 BYTE)       NULL,
    USERNAME        VARCHAR2(16 BYTE)       NOT NULL,
    ERROR_MESSAGES  VARCHAR2(100 BYTE)      NULL
)
