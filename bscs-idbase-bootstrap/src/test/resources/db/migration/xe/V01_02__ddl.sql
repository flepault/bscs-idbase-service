CREATE or REPLACE package  IDBASEBSCS.IDBASE
as
    procedure UpdateBscs
    (pinOperationId in integer,
     pisPhoneNum in varchar2,
     pisPhoneId in varchar2,
     pisTypeLabel in  varchar2,
     pisCivility in varchar2,
     pisLastName in varchar2,
     pisFirstName in varchar2,
     pisBirthDate in varchar2,
     pisBirthPlace in varchar2,
     pisPostlAdress in varchar2,
     pisOfficePhone in varchar2,
     pisHomePhone in varchar2,
     pisEmail in varchar2,
     pisTown in varchar2,
     pisCommune in varchar2,
     pisBlock in varchar2,
     pisOccupation in varchar2,
     pisScanUser in varchar2,
     pionResId in integer default 0,
     pionOMStatus in integer default -1,
     posMsisdn out varchar2,
     posSIM out varchar2);

end IDBASE;

