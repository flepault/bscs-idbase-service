################
# INSTALLATION #
################

1- Java version should be 1.8
2- Following environment variable should be set :
	- SOISRV_DATABASE_SERVER
	- SOISRV_DATABASE_PORT
	- ORACLE_SID
	- IDBASE_SERVER_HOST
3- Deploy & Unzip bscs-idbase-bootstrap-1.0.0.zip
4- To run BscsIdbaseService use : startBscsIdbaseService.sh

#############
# LOG LEVEL #
#############

To increase log level from INFO to DEBUG, you can update LOG_LEVEL value in following files:
- startBscsIdbaseService.sh

##################
# DATABASE LOGIN #
##################

To set the user/password of the database, you can update DATABASE_USER and DATABASE_PASSWORD value in following files:
- startBscsIdbaseService.sh

################################
# API REST BSCS IDBASE SERVICE #
################################

http://${HOST}:19092/bscs/idbase/swagger-ui/index.html

