package com.fl.bscs.idbase.data.soap.client.idbase;

import com.fl.bscs.idbase.data.soap.ws.skeleton.MySoapServerBindingStub;
import com.fl.bscs.idbase.data.soap.ws.skeleton.NumberCriteria;
import com.fl.bscs.idbase.data.soap.ws.skeleton.NumberStatusResult;
import com.fl.bscs.idbase.domain.aggregate.BscsIdbaseRequest;
import com.fl.bscs.idbase.domain.repository.IDBaseClient;
import lombok.AccessLevel;
import lombok.experimental.FieldDefaults;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import javax.xml.rpc.Stub;

@Slf4j
@Component
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
public class IDBaseClientAdapter implements IDBaseClient {

    MySoapServerBindingStub stub;

    public IDBaseClientAdapter(IDBaseClientProperties properties) throws Exception {
        this.stub = new MySoapServerBindingStub();
        this.stub._setProperty(javax.xml.rpc.Stub.ENDPOINT_ADDRESS_PROPERTY, properties.getCompleteUrl());
        this.stub._setProperty(Stub.USERNAME_PROPERTY, properties.getLogin());
        this.stub._setProperty(Stub.PASSWORD_PROPERTY, properties.getPassword());
    }

    //private static final String LOG_QUERY = "Querying from {} with url {}.";

    @Override
    public void setOperationOnNumber(BscsIdbaseRequest bscsIdbaseRequest) throws Exception {

        //LOG.info(LOG_QUERY, this.getClass().getSimpleName(), stub._getProperty(javax.xml.rpc.Stub.ENDPOINT_ADDRESS_PROPERTY));

        try{
            NumberCriteria numberCriteria = new NumberCriteria();
            numberCriteria.setMsisdn(bscsIdbaseRequest.getMsisdn() != null ? bscsIdbaseRequest.getMsisdn() : "");
            numberCriteria.setOldMsisdn(bscsIdbaseRequest.getOldMsisdn() != null ? bscsIdbaseRequest.getOldMsisdn() : "" );
            numberCriteria.setSim(bscsIdbaseRequest.getSim() != null ? bscsIdbaseRequest.getSim() : "");
            numberCriteria.setOldSim(bscsIdbaseRequest.getOldSim() != null ? bscsIdbaseRequest.getOldSim() : "");
            numberCriteria.setPartyId(bscsIdbaseRequest.getPartyId() != null ? bscsIdbaseRequest.getPartyId() : "");
            numberCriteria.setProcessName(bscsIdbaseRequest.getProcessName().toString());
            numberCriteria.setUser(bscsIdbaseRequest.getUsername());

            NumberStatusResult result = stub.setOperationOnNumber(numberCriteria);

            if(result.getStatus() != null && result.getStatus().equals("FAILED")) {
                LOG.error("Erreur lors du traitement dans IDBASE : "+result.getError());
                throw new Exception(result.getError());
            }
        } catch (Exception exception) {
            LOG.error(exception.getMessage());
            throw exception;
        }

    }
}
