package com.fl.bscs.idbase.data.soap.properties;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.FieldDefaults;

@Getter
@Setter
public class ClientProperties {

    String url;

    String endpoint;

    public String getCompleteUrl() {
        return url + endpoint;
    }
}
