package com.fl.bscs.idbase.data.soap.client.idbase;

import com.fl.bscs.idbase.data.soap.properties.ClientProperties;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.FieldDefaults;
import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties(prefix = "idbase.api")
@Getter
@Setter
public class IDBaseClientProperties extends ClientProperties {

    String login;

    String password;

}
