package com.fl.bscs.idbase.application.usecase;

import com.fl.bscs.idbase.domain.aggregate.BscsIdbaseRequest;
import com.fl.bscs.idbase.domain.repository.IDBaseClient;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.experimental.FieldDefaults;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Slf4j
@Service
@AllArgsConstructor
@FieldDefaults(makeFinal = true, level = AccessLevel.PRIVATE)
public class BscsIdbaseRequestUsecase {

    IDBaseClient idBaseClient;

    @Transactional
    public void mettreAJourBscsIdbaseRequest(BscsIdbaseRequest idbaseRequest) throws Exception {
        idBaseClient.setOperationOnNumber(idbaseRequest);
    }

}
