package com.fl.bscs.idbase.application.usecase;

import com.fl.bscs.idbase.domain.service.UpdateBscsService;
import com.fl.bscs.idbase.domain.vo.UpdateBscsDTO;
import com.fl.bscs.idbase.domain.vo.UpdateBscsResultDTO;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.experimental.FieldDefaults;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Slf4j
@Service
@AllArgsConstructor
@FieldDefaults(makeFinal = true, level = AccessLevel.PRIVATE)
public class UpdateBscsUsecase {

    UpdateBscsService updateBscsService;

    @Transactional
    public UpdateBscsResultDTO updateBscs(UpdateBscsDTO updateBscsDTO) {
        return updateBscsService.updateBscs(updateBscsDTO);
    }

}
