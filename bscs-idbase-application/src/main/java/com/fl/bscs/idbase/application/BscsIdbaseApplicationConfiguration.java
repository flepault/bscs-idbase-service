package com.fl.bscs.idbase.application;

import com.fl.bscs.idbase.domain.BscsIdbaseDomainConfiguration;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

@Configuration
@ComponentScan
@Import(BscsIdbaseDomainConfiguration.class)
public class BscsIdbaseApplicationConfiguration {
}
