#!/bin/bash

export BSCS_IDBASE_SERVICE_PORT=19092

export DATABASE_SERVER=${SOISRV_DATABASE_SERVER}
export DATABASE_PORT=${SOISRV_DATABASE_PORT}
export ORACLE_SID=${ORACLE_SID}

export DATABASE_USER=IDBASEBSCS
export DATABASE_PASSWORD=IDBASEBSCS

export IDBASE_SERVER=${IDBASE_SERVER_HOST}
export IDBASE_SOAP_PATH=/maarch_entreprise/modules/web_service/server
export IDBASE_LOGIN=idbase
export IDBASE_PASSWORD=idbase

export LOG_LEVEL=INFO

echo "BscsIdbaseService is starting..."

nohup ./bscs-idbase-bootstrap.jar --spring.profiles.active=prod&

sleep 5
MEP_PID=`ps -edf | grep $LOGNAME | grep "java" | grep "bscs-idbase-bootstrap"| awk '{print $2}'`

echo "BscsIdbaseService is started, pid:${MEP_PID}"
