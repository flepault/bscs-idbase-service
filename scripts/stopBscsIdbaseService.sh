#!/bin/bash

PP_PID=`ps -edf | grep $LOGNAME | grep "java" | grep "bscs-idbase-bootstrap"| awk '{print $2}'`

echo "BscsIdbaseService is stopping..."

kill -9 ${PP_PID}

sleep 5

echo "BscsIdbaseService is stopped"
