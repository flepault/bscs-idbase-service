package com.fl.bscs.idbase.rest.endpoint.update.bscs;

import com.fl.bscs.idbase.application.usecase.UpdateBscsUsecase;
import com.fl.bscs.idbase.domain.vo.UpdateBscsDTO;
import com.fl.bscs.idbase.domain.vo.UpdateBscsResultDTO;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(UpdateBscsEndpoint.UPDATE_BSCS_ENDPOINT)
@AllArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
@Component
public class UpdateBscsEndpoint {

    UpdateBscsUsecase updateBscsUsecase;

    public static final String UPDATE_BSCS_ENDPOINT = "/rest/api/update-bscs";

    @PostMapping()
    public ResponseEntity<UpdateBscsResultDTO> updateBscs(@RequestBody UpdateBscsDTO updateBscsDTO) {
        try {
            return ResponseEntity.ok(updateBscsUsecase.updateBscs(updateBscsDTO));
        } catch (Exception exception) {
            return new ResponseEntity(exception.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }
}
