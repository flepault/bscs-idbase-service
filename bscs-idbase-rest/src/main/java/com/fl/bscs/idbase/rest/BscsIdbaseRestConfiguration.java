package com.fl.bscs.idbase.rest;

import com.fl.bscs.idbase.application.BscsIdbaseApplicationConfiguration;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

@Configuration
@ComponentScan
@Import(
        BscsIdbaseApplicationConfiguration.class
)
public class BscsIdbaseRestConfiguration {
}
