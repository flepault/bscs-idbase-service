package com.fl.bscs.idbase.rest.endpoint.admin;

import com.fl.bscs.idbase.domain.aggregate.BscsIdbaseRequest;
import com.fl.bscs.idbase.domain.repository.BscsIdbaseRequestRepository;
import com.fl.bscs.idbase.domain.vo.ProcessName;
import com.fl.bscs.idbase.domain.vo.Status;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDateTime;
import java.util.List;

@RestController
@RequestMapping(BscsIdbaseRequestEndpoint.IDBASE_ENDPOINT)
@AllArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
@Component
public class BscsIdbaseRequestEndpoint {

    BscsIdbaseRequestRepository bscsIdbaseRequestRepository;

    static final String IDBASE_ENDPOINT = "/rest/api/idbase-request";

    @GetMapping()
    @Transactional
    public ResponseEntity<List<BscsIdbaseRequest>> getAllIdBaseRequest() {
        return ResponseEntity.ok(bscsIdbaseRequestRepository.findAll());
    }

    @GetMapping("/test")
    @Transactional
    public ResponseEntity<List<BscsIdbaseRequest>> testIdBaseRequest() {

        bscsIdbaseRequestRepository.save(BscsIdbaseRequest.of(1L, 18097L, LocalDateTime.now(), Status.NOUVEAU, LocalDateTime.now(), ProcessName.changeSim, "USER").setMsisdn("123456789"));
        bscsIdbaseRequestRepository.save(BscsIdbaseRequest.of(2L, 19050L, LocalDateTime.now(), Status.NOUVEAU, LocalDateTime.now(), ProcessName.changeMsisdn, "USER").setMsisdn("9887654321"));
        bscsIdbaseRequestRepository.save(BscsIdbaseRequest.of(3L, 19050L, LocalDateTime.now(), Status.NOUVEAU, LocalDateTime.now(), ProcessName.changeMsisdn, "USER"));

        return ResponseEntity.ok(bscsIdbaseRequestRepository.findAll());
    }
}
